<footer class="footer is-primary">
  <div class="box">
    <div class="content has-text-centered">
      <p>
        <strong>Employee Management System by <a href="https://bitbucket.org/racann/">Ryan Cann</a></strong>
      </p>
    </div>
  </div>
</footer>

