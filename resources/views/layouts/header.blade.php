<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="/">
      <i class="fa fa-home"></i>
    </a>

    <a  class="navbar-item" href="/timeclock"><i class="fa fa-clock-o"></i></a>
    <a  class="navbar-item" href="/shifts"><i class="fa fa-shopping-bag"></i></a>
    <a  class="navbar-item" href="#"><i class="fa fa-question-circle"></i></a>

    <div class="navbar-burger">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
</nav>

<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="/manager">
     Home
    </a>

    <a  class="navbar-item" href="/employees/">Employees</a>
    <a  class="navbar-item" href="/shifts">Shifts</a>
    <a  class="navbar-item" href="#">Orders</a>
    <a  class="navbar-item" href="/products">Inventory</a>

    <div class="navbar-burger">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
</nav>

{{--old nav end--}}