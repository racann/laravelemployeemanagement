<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="/">
            <i class="fa fa-home"></i>
        </a>

        <a  class="navbar-item" href="/shift/new"><i class="fa fa-clock-o"></i></a>
        <a  class="navbar-item" href="/shifts"><i class="fa fa-shopping-bag"></i></a>
        <a  class="navbar-item" href="#"><i class="fa fa-question-circle"></i></a>

        <div class="navbar-burger">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</nav>