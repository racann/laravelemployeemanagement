<?php

namespace App\Http\Controllers;

use App\PaySchedule;
use Illuminate\Http\Request;

class PayScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paySchedules = PaySchedule::all();

        return view('payschedules.index', compact('paySchedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('payschedules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request([
            ''
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaySchedule  $paySchedule
     * @return \Illuminate\Http\Response
     */
    public function show(PaySchedule $paySchedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaySchedule  $paySchedule
     * @return \Illuminate\Http\Response
     */
    public function edit(PaySchedule $paySchedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaySchedule  $paySchedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaySchedule $paySchedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaySchedule  $paySchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaySchedule $paySchedule)
    {
        //
    }
}
