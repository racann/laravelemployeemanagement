<?php

namespace App\Http\Controllers;

use App\ScheduleSetting;
use Illuminate\Http\Request;

class ScheduleSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ScheduleSetting  $scheduleSetting
     * @return \Illuminate\Http\Response
     */
    public function show(ScheduleSetting $scheduleSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ScheduleSetting  $scheduleSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(ScheduleSetting $scheduleSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ScheduleSetting  $scheduleSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ScheduleSetting $scheduleSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ScheduleSetting  $scheduleSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(ScheduleSetting $scheduleSetting)
    {
        //
    }
}
